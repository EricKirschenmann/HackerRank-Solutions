﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACMICPCTeam
{
    class Program
    {
        // Complete the acmTeam function below.
        static int[] AcmTeam(string[] topic)
        {
            int max = 0;
            int count = 0;

            for (int x = 0; x < topic.Length - 1; x++)
            {
                for (int y = x + 1; y < topic.Length; y++)
                {
                    int ones = 0;

                    // go through the two strings and count the total number of ones that occur between them
                    for (int z = 0; z < topic[x].Length; z++)
                    {
                        if (topic[x][z] != topic[y][z] || topic[x][z] == '1' && topic[y][z] == '1')
                        {
                            ones++;
                        }
                    }

                    // check if current amount of ones is a new max
                    if (ones > max)
                    {
                        max = ones;
                        count = 1;
                    }
                    else if (ones == max)
                    {
                        // if the current amount of ones is the same as the previous max, count it
                        count++;
                    }
                }
            }

            return new int[] { max, count };
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            string[] nm = Console.ReadLine().Split(' ');

            int n = Convert.ToInt32(nm[0]);

            int m = Convert.ToInt32(nm[1]);

            string[] topic = new string[n];

            for (int i = 0; i < n; i++)
            {
                string topicItem = Console.ReadLine();
                topic[i] = topicItem;
            }

            int[] result = AcmTeam(topic);

            Console.WriteLine(string.Join("\n", result));

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
