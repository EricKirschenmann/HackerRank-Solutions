﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngryProfessor
{
    class Program
    {
        // Complete the angryProfessor function below.
        static string AngryProfessor(int k, int[] a)
        {
            int onTime = 0;

            // count how many students made it in on time
            for (int x = 0; x < a.Length; x++)
            {
                if (a[x] <= 0)
                {
                    onTime++;
                }
            }

            // check if class is canceled or not
            return (onTime < k)
                ? "YES"
                : "NO";
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int t = Convert.ToInt32(Console.ReadLine());

            for (int tItr = 0; tItr < t; tItr++)
            {
                string[] nk = Console.ReadLine().Split(' ');

                int n = Convert.ToInt32(nk[0]);

                int k = Convert.ToInt32(nk[1]);

                int[] a = Array.ConvertAll(Console.ReadLine().Split(' '), aTemp => Convert.ToInt32(aTemp));
                string result = AngryProfessor(k, a);

                Console.WriteLine(result);
            }

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
