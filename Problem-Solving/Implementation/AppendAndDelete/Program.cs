﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppendAndDelete
{
    class Program
    {
        // Complete the appendAndDelete function below.
        static string AppendAndDelete(string s, string t, int k)
        {
            int same = 0;

            // find how many they have in common
            for (int x = 0; x < Math.Min(s.Length, t.Length); x++)
            {
                if (s[x] == t[x])
                {
                    same++;
                }
                else
                {
                    break;
                }
            }

            // check conditions
            return (s.Length + t.Length - 2 * same) > k
                ? "No"
                : (s.Length + t.Length - 2 * same) % 2 == k % 2 ? "Yes" : (s.Length + t.Length - k) < 0 ? "Yes" : "No";
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            string s = Console.ReadLine();

            string t = Console.ReadLine();

            int k = Convert.ToInt32(Console.ReadLine());

            string result = AppendAndDelete(s, t, k);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
