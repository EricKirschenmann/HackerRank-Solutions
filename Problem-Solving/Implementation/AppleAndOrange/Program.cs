﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppleAndOrange
{
    class Program
    {
        // Complete the countApplesAndOranges function below.
        static void CountApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges)
        {
            int numA = 0;
            int numO = 0;
            int position = 0;

            // check the position of all the fallen apples
            for (int x = 0; x < apples.Length; x++)
            {
                position = apples[x] + a;
                if (position >= s && position <= t)
                {
                    numA++;
                }
            }

            // check the position of all the fallen oranges
            for (int x = 0; x < oranges.Length; x++)
            {
                position = oranges[x] + b;
                // fell towards the house
                if (position >= s && position <= t)
                {
                    numO++;
                }
            }

            Console.WriteLine(numA);
            Console.WriteLine(numO);
        }

        static void Main(string[] args)
        {
            string[] st = Console.ReadLine().Split(' ');

            int s = Convert.ToInt32(st[0]);

            int t = Convert.ToInt32(st[1]);

            string[] ab = Console.ReadLine().Split(' ');

            int a = Convert.ToInt32(ab[0]);

            int b = Convert.ToInt32(ab[1]);

            string[] mn = Console.ReadLine().Split(' ');

            int m = Convert.ToInt32(mn[0]);

            int n = Convert.ToInt32(mn[1]);

            int[] apples = Array.ConvertAll(Console.ReadLine().Split(' '), applesTemp => Convert.ToInt32(applesTemp));

            int[] oranges = Array.ConvertAll(Console.ReadLine().Split(' '), orangesTemp => Convert.ToInt32(orangesTemp));

            CountApplesAndOranges(s, t, a, b, apples, oranges);
        }
    }
}
