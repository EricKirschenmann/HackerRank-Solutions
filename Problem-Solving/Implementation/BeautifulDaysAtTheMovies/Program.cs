﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeautifulDaysAtTheMovies
{
    class Program
    {
        // Complete the beautifulDays function below.
        static int BeautifulDays(int i, int j, int k)
        {
            int count = 0;

            // loop through days
            for (; i <= j; i++)
            {
                // reverse the current day
                int reverse = Convert.ToInt32(new string(i.ToString().ToCharArray().Reverse().ToArray()));

                // if k is a factor of the difference of the original and reverse count
                if (Math.Abs(i - reverse) % k == 0)
                {
                    count++;
                }
            }

            return count;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            string[] ijk = Console.ReadLine().Split(' ');

            int i = Convert.ToInt32(ijk[0]);

            int j = Convert.ToInt32(ijk[1]);

            int k = Convert.ToInt32(ijk[2]);

            int result = BeautifulDays(i, j, k);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
