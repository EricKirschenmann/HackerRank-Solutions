﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetweenTwoSets
{
    class Program
    {
        /*
         * Complete the getTotalX function below.
         */
        static int GetTotalX(int[] a, int[] b)
        {
            /*
             * Write your code here.
             */
            // get starting values
            int f = LCM(a);
            int l = GCD(b);
            int count = 0;

            // loop through and count the number of multiples
            // of the LCM that divide evenly into the GCD
            for (int x = f, j = 2; x <= l; x = f * j, j++)
            {
                if (l % x == 0)
                {
                    count++;
                }
            }

            return count;
        }

        // Finds the greatest common denominator between two integers
        static int GCD(int a, int b)
        {
            while (b > 0)
            {
                int temp = b;
                b = a % b;
                a = temp;
            }

            return a;
        }

        // finds the greatest common denominator between all the elements in an array
        static int GCD(int[] input)
        {
            int result = input[0];
            for (int x = 1; x < input.Length; x++)
            {
                result = GCD(result, input[x]);
            }
            return result;
        }

        // finds the least common multiple between two integers
        static int LCM(int a, int b)
        {
            return a * (b / GCD(a, b));
        }

        // finds the least common multiple between all the elements in an array
        static int LCM(int[] input)
        {
            int result = input[0];
            for (int x = 1; x < input.Length; x++)
            {
                result = LCM(result, input[x]);
            }
            return result;
        }

        static void Main(string[] args)
        {
            //TextWriter tw = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            string[] nm = Console.ReadLine().Split(' ');

            int n = Convert.ToInt32(nm[0]);

            int m = Convert.ToInt32(nm[1]);

            int[] a = Array.ConvertAll(Console.ReadLine().Split(' '), aTemp => Convert.ToInt32(aTemp));

            int[] b = Array.ConvertAll(Console.ReadLine().Split(' '), bTemp => Convert.ToInt32(bTemp));

            int total = GetTotalX(a, b);

            Console.WriteLine(total);

            //tw.Flush();
            //tw.Close();
        }
    }
}
