﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdayChocolate
{
    class Program
    {
        // Complete the solve function below.
        static int Solve(int[] s, int d, int m)
        {
            int count = 0;

            for (int x = 0; x <= s.Length - m; x++)
            {
                int temp = 0;

                // loop through sub-arrays with the length of m
                for (int y = x; y < x + m; y++)
                {
                    temp += s[y];
                }

                // if the sum of the sub-array matches the day
                if (temp == d)
                {
                    count++;
                }
            }

            return count;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int n = Convert.ToInt32(Console.ReadLine());

            int[] s = Array.ConvertAll(Console.ReadLine().Split(' '), sTemp => Convert.ToInt32(sTemp));
            string[] dm = Console.ReadLine().Split(' ');

            int d = Convert.ToInt32(dm[0]);

            int m = Convert.ToInt32(dm[1]);

            int result = Solve(s, d, m);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
