﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BonAppetit
{
    class Program
    {
        static string BonAppetit(int k, int[] arr, int b)
        {
            int split = (arr.Sum() - arr[k]) / 2;

            return (split >= b) ? "Bon Appetit" : (b - split).ToString();
        }

        static void Main(string[] args)
        {
            /* Enter your code here. Read input from STDIN. Print output to STDOUT */
            string[] nk = Console.ReadLine().Split(' ');

            // how many items
            int n = Convert.ToInt32(nk[0]);

            // zero based index of item not eaten
            int k = Convert.ToInt32(nk[1]);

            // array of the cost of the items
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arTemp => Convert.ToInt32(arTemp));

            // amount charged
            int b = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(BonAppetit(k, arr, b));
        }
    }
}
