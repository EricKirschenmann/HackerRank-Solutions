﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakingTheRecords
{
    class Program
    {
        // Complete the breakingRecords function below.
        static int[] BreakingRecords(int[] scores)
        {
            int max = scores[0];
            int min = scores[0];

            int maxCount = 0;
            int minCount = 0;

            // loop through the scores and count how many times each record changes
            for (int x = 0; x < scores.Length; x++)
            {
                if (scores[x] > max)
                {
                    max = scores[x];
                    maxCount++;
                }
                else if (scores[x] < min)
                {
                    min = scores[x];
                    minCount++;
                }
            }

            // return the number of changes
            return new int[] { maxCount, minCount };
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int n = Convert.ToInt32(Console.ReadLine());

            int[] scores = Array.ConvertAll(Console.ReadLine().Split(' '), scoresTemp => Convert.ToInt32(scoresTemp));
            int[] result = BreakingRecords(scores);

            Console.WriteLine(string.Join(" ", result));

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
