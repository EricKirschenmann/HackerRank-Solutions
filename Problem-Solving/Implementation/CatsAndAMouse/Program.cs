﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatsAndAMouse
{
    class Program
    {
        // Complete the catAndMouse function below.
        static string CatAndMouse(int x, int y, int z)
        {
            // check the distances between them and print the cat that is closer
            return (Math.Abs(x - z) == Math.Abs(y - z)) 
                ? "Mouse C" 
                : (Math.Abs(x - z) < Math.Abs(y - z)) 
                ? "Cat A" 
                : "Cat B";
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int q = Convert.ToInt32(Console.ReadLine());

            for (int qItr = 0; qItr < q; qItr++)
            {
                string[] xyz = Console.ReadLine().Split(' ');

                int x = Convert.ToInt32(xyz[0]);

                int y = Convert.ToInt32(xyz[1]);

                int z = Convert.ToInt32(xyz[2]);

                string result = CatAndMouse(x, y, z);

                Console.WriteLine(result);
            }

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
