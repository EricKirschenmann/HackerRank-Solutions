﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircularArrayRotation
{
    class Program
    {
        // Complete the circularArrayRotation function below.
        static int[] CircularArrayRotation(int[] a, int k, int[] queries)
        {
            int[] answers = new int[queries.Length];

            // loop through the queries
            for (int x = 0; x < queries.Length; x++)
            {
                // "rotate" the array by k, then get the result at the current query position
                answers[x] = a[(a.Length - (k % a.Length) + queries[x]) % a.Length];
            }

            return answers;

        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            string[] nkq = Console.ReadLine().Split(' ');

            int n = Convert.ToInt32(nkq[0]);

            int k = Convert.ToInt32(nkq[1]);

            int q = Convert.ToInt32(nkq[2]);

            int[] a = Array.ConvertAll(Console.ReadLine().Split(' '), aTemp => Convert.ToInt32(aTemp));

            int[] queries = new int[q];

            for (int i = 0; i < q; i++)
            {
                int queriesItem = Convert.ToInt32(Console.ReadLine());
                queries[i] = queriesItem;
            }

            int[] result = CircularArrayRotation(a, k, queries);

            Console.WriteLine(string.Join("\n", result));

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
