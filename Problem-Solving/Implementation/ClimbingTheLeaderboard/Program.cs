﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClimbingTheLeaderboard
{
    class Program
    {
        // Complete the climbingLeaderboard function below.
        static int[] ClimbingLeaderboard(int[] scores, int[] alice)
        {
            Stack<int> ranks = new Stack<int>();
            int[] temp = new int[alice.Length];
            
            // copy distinct scores into the stack
            for (int x = 0; x < scores.Length; x++)
            {
                if (ranks.Count == 0 || ranks.Peek() != scores[x])
                {
                    ranks.Push(scores[x]);
                }
            }

            // go through the stack and find the rank of alice's score by popping off lower scores
            for (int x = 0; x < alice.Length; x++)
            {
                while (!(ranks.Count == 0) && alice[x] >= ranks.Peek())
                {
                    ranks.Pop();
                }
                temp[x] = ranks.Count + 1;
            }

            return temp;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int scoresCount = Convert.ToInt32(Console.ReadLine());

            int[] scores = Array.ConvertAll(Console.ReadLine().Split(' '), scoresTemp => Convert.ToInt32(scoresTemp));

            int aliceCount = Convert.ToInt32(Console.ReadLine());

            int[] alice = Array.ConvertAll(Console.ReadLine().Split(' '), aliceTemp => Convert.ToInt32(aliceTemp));

            int[] result = ClimbingLeaderboard(scores, alice);

            Console.WriteLine(string.Join("\n", result));

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
