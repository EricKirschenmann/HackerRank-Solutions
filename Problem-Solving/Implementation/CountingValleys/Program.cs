﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountingValleys
{
    class Program
    {
        // Complete the countingValleys function below.
        static int CountingValleys(int n, string s)
        {
            int elevation = 0;
            int count = 0;

            for (int x = 0; x < s.Length; x++)
            {
                // move up or down
                if (s[x] == 'U')
                {
                    elevation++;
                }
                else
                {
                    elevation--;
                }

                // if coming up to 0
                if (elevation == 0 && s[x] == 'U')
                {
                    count++;
                }
            }

            return count;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int n = Convert.ToInt32(Console.ReadLine());

            string s = Console.ReadLine();

            int result = CountingValleys(n, s);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
