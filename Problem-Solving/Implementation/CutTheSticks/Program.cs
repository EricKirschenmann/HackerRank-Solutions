﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CutTheSticks
{
    class Program
    {
        // Complete the cutTheSticks function below.
        static int[] CutTheSticks(int[] arr)
        {
            // sort the array
            Array.Sort(arr);

            // going to need an array to hold a number for however many distinct values
            int[] counts = new int[arr.Distinct().Count()];

            // store the first count of items
            int index = 0;
            counts[index] = arr.Length;

            // loop through the array
            for (int x = 1; x < arr.Length; x++)
            {
                // if there is a different value
                if (arr[x] != arr[x - 1])
                {
                    // count the total excluding the old difference
                    index++;
                    counts[index] = arr.Length - x;
                }
            }

            return counts;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int n = Convert.ToInt32(Console.ReadLine());

            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            int[] result = CutTheSticks(arr);

            Console.WriteLine(string.Join("\n", result));

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
