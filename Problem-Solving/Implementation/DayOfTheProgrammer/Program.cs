﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DayOfTheProgrammer
{
    class Program
    {
        // Complete the solve function below.
        static string Solve(int year)
        {
            /* *
             * Jan = 31, Feb = 28, Mar = 31, Apr = 30, May = 31, Jun = 30
             * Jul = 31, Aug = 31, Sep = 30, Oct = 31, Nov = 30, Dec = 31 
             * */

            return year == 1918
                ? "26.09.1918"
                : ((year <= 1917) & (year % 4 == 0)) || ((year > 1918) & (year % 400 == 0 || ((year % 4 == 0) & (year % 100 != 0))))
                    ? "12.09." + year
                    : "13.09." + year;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int year = Convert.ToInt32(Console.ReadLine());

            string result = Solve(year);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
