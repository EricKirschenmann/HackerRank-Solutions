﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignerPDFViewer
{
    class Program
    {
        // Complete the designerPdfViewer function below.
        static int DesignerPdfViewer(int[] h, string word)
        {
            int max = 0;

            // get the height of the tallest letter in the word
            for (int x = 0; x < word.Length; x++)
            {
                max = Math.Max(max, h[word[x] - 97]);
            }

            // return height * length to calculate the area
            return max * word.Length;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int[] h = Array.ConvertAll(Console.ReadLine().Split(' '), hTemp => Convert.ToInt32(hTemp));
            string word = Console.ReadLine();

            int result = DesignerPdfViewer(h, word);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
