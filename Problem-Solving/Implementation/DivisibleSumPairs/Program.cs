﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DivisibleSumPairs
{
    class Program
    {
        // Complete the divisibleSumPairs function below.
        static int DivisibleSumPairs(int n, int k, int[] ar)
        {
            int count = 0;

            // loop through main array
            for (int x = 0; x < ar.Length; x++)
            {
                // loop through sub-array
                for (int y = x + 1; y < ar.Length; y++)
                {
                    // check if divisible by k
                    if ((ar[x] + ar[y]) % k == 0)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            string[] nk = Console.ReadLine().Split(' ');

            int n = Convert.ToInt32(nk[0]);

            int k = Convert.ToInt32(nk[1]);

            int[] ar = Array.ConvertAll(Console.ReadLine().Split(' '), arTemp => Convert.ToInt32(arTemp));
            int result = DivisibleSumPairs(n, k, ar);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
