﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicsShop
{
    class Program
    {
        /*
         * Complete the getMoneySpent function below.
         */
        static int GetMoneySpent(int[] keyboards, int[] drives, int b)
        {
            /*
             * Write your code here.
             */
            int current, max = 0;

            for (int x = 0; x < keyboards.Length; x++)
            {
                for (int y = 0; y < drives.Length; y++)
                {
                    current = keyboards[x] + drives[y];

                    if (current <= b)
                    {
                        max = Math.Max(max, current);
                    }
                }
            }

            return max != 0 ? max : -1;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            string[] bnm = Console.ReadLine().Split(' ');

            int b = Convert.ToInt32(bnm[0]);

            int n = Convert.ToInt32(bnm[1]);

            int m = Convert.ToInt32(bnm[2]);

            int[] keyboards = Array.ConvertAll(Console.ReadLine().Split(' '), keyboardsTemp => Convert.ToInt32(keyboardsTemp));

            int[] drives = Array.ConvertAll(Console.ReadLine().Split(' '), drivesTemp => Convert.ToInt32(drivesTemp));
            /*
             * The maximum amount of money she can spend on a keyboard and USB drive, or -1 if she can't purchase both items
             */

            int moneySpent = GetMoneySpent(keyboards, drives, b);

            Console.WriteLine(moneySpent);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
