﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EqualizeTheArray
{
    class Program
    {
        // Complete the equalizeArray function below.
        static int EqualizeArray(int[] arr)
        {
            int mode = arr.GroupBy(v => v)
                .OrderByDescending(g => g.Count())
                .First()
                .Key;

            return arr.Length - mode;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int n = Convert.ToInt32(Console.ReadLine());

            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            int result = EqualizeArray(arr);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
