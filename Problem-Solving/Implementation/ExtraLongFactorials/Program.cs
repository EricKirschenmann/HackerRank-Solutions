﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ExtraLongFactorials
{
    class Program
    {
        // Complete the extraLongFactorials function below.
        static void ExtraLongFactorials(int n)
        {
            // need to store more than 64 bit integers
            BigInteger bigInteger = 1;

            // calculate factorial
            while(n > 0)
            {
                bigInteger *= n;
                n--;
            }

            Console.WriteLine(bigInteger);
        }

        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());

            ExtraLongFactorials(n);
        }
    }
}
