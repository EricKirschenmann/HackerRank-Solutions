﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindDigits
{
    class Program
    {
        // Complete the findDigits function below.
        static int FindDigits(int n)
        {
            int count = 0;
            string temp = n.ToString();

            for (int x = 0; x < temp.Length; x++)
            {
                // get the current integer
                int l = Convert.ToInt32(temp[x].ToString());

                // don't divide by zero
                if (l == 0)
                {
                    continue;
                }

                // check if it's a factor
                if (n % l == 0)
                {
                    count++;
                }
            }

            return count;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int t = Convert.ToInt32(Console.ReadLine());

            for (int tItr = 0; tItr < t; tItr++)
            {
                int n = Convert.ToInt32(Console.ReadLine());

                int result = FindDigits(n);

                Console.WriteLine(result);
            }

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
