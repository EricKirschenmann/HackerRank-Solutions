﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormingAMagicSquare
{
    class Program
    {
        // Complete the formingMagicSquare function below.
        static int FormingMagicSquare(List<int> square)
        {
            // all magic squares
            int[][] allMagics = new int[][]
            {
                new int[] { 8, 1, 6, 3, 5, 7, 4, 9, 2},
                new int[] { 6, 1, 8, 7, 5, 3, 2, 9, 4},
                new int[] { 4, 9, 2, 3, 5, 7, 8, 1, 6},
                new int[] { 2, 9, 4, 7, 5, 3, 6, 1, 8},
                new int[] { 8, 3, 4, 1, 5, 9, 6, 7, 2},
                new int[] { 4, 3, 8, 9, 5, 1, 2, 7, 6},
                new int[] { 6, 7, 2, 1, 5, 9, 8, 3, 4},
                new int[] { 2, 7, 6, 9, 5, 1, 4, 3, 8}
            };

            // calculate difference between given square and closest Magic
            return allMagics.Select(ar => ar.Zip(square, (a, b) => Math.Abs(a - b)).Sum()).Min();
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            List<int> square = new List<int>();

            for (int i = 0; i < 3; i++)
            {
                string[] temp = Console.ReadLine().Split(' ');
                foreach (string item in temp)
                {
                    square.Add(Convert.ToInt32(item));
                }
            }

            int result = FormingMagicSquare(square);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
