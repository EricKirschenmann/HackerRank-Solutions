﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradingStudents
{
    class Program
    {
        /*
         * Complete the gradingStudents function below.
         */
        static int[] GradingStudents(int[] grades)
        {
            /*
             * Write your code here.
             */
            for (int x = 0; x < grades.Length; x++)
            {
                if (grades[x] >= 38)
                {
                    grades[x] = grades[x] % 5 < 3
                        ? grades[x]
                        : (grades[x] + (5 - (grades[x] % 5)));
                }
            }

            return grades;
        }

        static void Main(string[] args)
        {
            //TextWriter tw = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int n = Convert.ToInt32(Console.ReadLine());

            int[] grades = new int[n];

            for (int gradesItr = 0; gradesItr < n; gradesItr++)
            {
                int gradesItem = Convert.ToInt32(Console.ReadLine());
                grades[gradesItr] = gradesItem;
            }

            int[] result = GradingStudents(grades);

            Console.WriteLine(string.Join("\n", result));

            //tw.Flush();
            //tw.Close();
        }
    }
}
