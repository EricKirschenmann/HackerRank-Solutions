﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JumpingOnTheClouds
{
    class Program
    {
        // Complete the jumpingOnClouds function below.
        static int JumpingOnClouds(int[] c)
        {
            int count = -1;

            // loop through the array (x acting as a jump to the next cloud)
            for (int x = 0; x < c.Length; x++)
            {
                // if it is possible to jump two and jumping two is safe
                if (x < c.Length - 2 && c[x + 2] == 0)
                {
                    // make second jump
                    x++;
                }
                // count number of jumps
                count++;
            }

            return count;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int n = Convert.ToInt32(Console.ReadLine());

            int[] c = Array.ConvertAll(Console.ReadLine().Split(' '), cTemp => Convert.ToInt32(cTemp));
            int result = JumpingOnClouds(c);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
