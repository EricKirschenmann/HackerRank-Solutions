﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JumpingOnTheCloudsRevisited
{
    class Program
    {
        // Complete the jumpingOnClouds function below.
        static int JumpingOnClouds(int[] c, int k)
        {
            int energy = 100;
            int cloud = 0;

            do
            {
                // change the current cloud
                cloud = (cloud + k) % c.Length;
                // remove energy required to get to that cloud
                energy--;

                // check if current cloud is thunder
                if (c[cloud] == 1)
                {
                    energy -= 2;
                }

            } while (cloud != 0); // keep looping until she returns to the start

            return energy;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            string[] nk = Console.ReadLine().Split(' ');

            int n = Convert.ToInt32(nk[0]);

            int k = Convert.ToInt32(nk[1]);

            int[] c = Array.ConvertAll(Console.ReadLine().Split(' '), cTemp => Convert.ToInt32(cTemp));
            int result = JumpingOnClouds(c, k);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
