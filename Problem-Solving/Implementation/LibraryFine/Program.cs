﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryFine
{
    class Program
    {
        // Complete the libraryFine function below.
        static int LibraryFine(int d1, int m1, int y1, int d2, int m2, int y2)
        {
            if (y1 > y2)
            {
                // if returned in a year later than the due year
                return 10000;
            }
            else if (y1 < y2)
            {
                // returned before the due year
                return 0;
            }
            else
            {
                // if returned during the same year
                if (m1 > m2)
                {
                    // if returned in a later month
                    return 500 * (m1 - m2);
                }
                else if (m1 == m2)
                {
                    // if returned in the same month, check if late on the days
                    return d1 > d2 ? 15 * (d1 - d2) : 0;
                }
                else
                {
                    // returned on or before due date
                    return 0;
                }
            }

        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            string[] d1M1Y1 = Console.ReadLine().Split(' ');

            int d1 = Convert.ToInt32(d1M1Y1[0]);

            int m1 = Convert.ToInt32(d1M1Y1[1]);

            int y1 = Convert.ToInt32(d1M1Y1[2]);

            string[] d2M2Y2 = Console.ReadLine().Split(' ');

            int d2 = Convert.ToInt32(d2M2Y2[0]);

            int m2 = Convert.ToInt32(d2M2Y2[1]);

            int y2 = Convert.ToInt32(d2M2Y2[2]);

            int result = LibraryFine(d1, m1, y1, d2, m2, y2);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
