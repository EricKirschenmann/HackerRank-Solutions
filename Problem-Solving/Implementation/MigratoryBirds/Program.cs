﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigratoryBirds
{
    class Program
    {
        // Complete the migratoryBirds function below.
        static int MigratoryBirds(int[] ar)
        {
            int[] counts = new int[5];

            // count total occurences of each
            for (int x = 0; x < ar.Length; x++)
            {
                counts[ar[x] - 1]++;
            }

            int max = int.MinValue;
            int index = -1;

            // find index of the max occurrrences
            for (int x = 0; x < counts.Length; x++)
            {
                if (max < counts[x])
                {
                    max = counts[x];
                    index = x + 1;
                }
            }

            return index;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int arCount = Convert.ToInt32(Console.ReadLine());

            int[] ar = Array.ConvertAll(Console.ReadLine().Split(' '), arTemp => Convert.ToInt32(arTemp));
            int result = MigratoryBirds(ar);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
