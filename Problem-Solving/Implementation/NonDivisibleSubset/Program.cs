﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NonDivisibleSubset
{
    class Program
    {
        // Complete the nonDivisibleSubset function below.
        static int NonDivisibleSubset(UInt64 k, UInt64[] arr)
        {
            int[] remainders = new int[k];
            int count = 0;

            // Count the # of numbers that have each remainder
            for (UInt64 i = 0; i < Convert.ToUInt64(arr.Length); i++)
            {
                remainders[arr[i] % k]++;
            }

            // Get our counts of each category
            for (UInt64 j = 1; j <= (k / 2); j++)
            {
                // if the remainders are the same (i.e. k / 2),
                // there can be only 1
                if (j == k - j)
                {
                    count++;
                    continue;
                }

                // Otherwise, add the remainder with the
                // highest count
                count += Math.Max(remainders[j], remainders[k - j]);
            }

            // If we had one or more evenly divisible numbers,
            // there can be only 1
            if (remainders[0] > 0)
                count++;

            return count;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            string[] nk = Console.ReadLine().Split(' ');

            UInt64 n = Convert.ToUInt64(nk[0]);

            UInt64 k = Convert.ToUInt64(nk[1]);

            UInt64[] S = Array.ConvertAll(Console.ReadLine().Split(' '), STemp => Convert.ToUInt64(STemp));
            int result = NonDivisibleSubset(k, S);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
