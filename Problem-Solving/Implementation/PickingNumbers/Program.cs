﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickingNumbers
{
    class Program
    {
        // Complete the pickingNumbers function below.
        static int PickingNumbers(int[] a)
        {
            int[] counts = new int[100];

            // get the count of each distinct value
            for (int x = 0; x < a.Length; x++)
            {
                counts[a[x]]++;
            }

            int max = 0;

            // get the max length of the array made by each pair of values
            for (int x = 0; x < counts.Length - 1; x++)
            {
                max = Math.Max(max, counts[x] + counts[x + 1]);
            }

            return max;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int n = Convert.ToInt32(Console.ReadLine());

            int[] a = Array.ConvertAll(Console.ReadLine().Split(' '), aTemp => Convert.ToInt32(aTemp));

            int result = PickingNumbers(a);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
