﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepeatedString
{
    class Program
    {
        // Complete the repeatedString function below.
        static long RepeatedString(string s, long n)
        {
            // get total amount of 'a' in the string
            int aCount = s.Count(c => c == 'a');

            // if all 'a' return the length
            if (s.Count(c => c != 'a') == 0)
            {
                return n;
            }

            // get how many times the full string will repeat
            long repeats = n / s.Length;

            // get how many characters of the string will need to be added on
            long remainder = n % s.Length;

            // get the amount of 'a' in the string that fills the remaining length
            int remainderCount = s.Substring(0, (int)(remainder)).Count(c => c == 'a');

            // get total of 'a' in full string
            return aCount * repeats + remainderCount;

            // unreadable one-liner
            //return s.Count(c => c != 'a') == 0 ? n : (s.Count(c => c == 'a') * (n / s.Length)) + s.Substring(0, (int)(n % s.Length)).Count(c => c == 'a');
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            string s = Console.ReadLine();

            long n = Convert.ToInt64(Console.ReadLine());

            long result = RepeatedString(s, n);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
