﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SequenceEquation
{
    class Program
    {
        // Complete the permutationEquation function below.
        static int[] PermutationEquation(int[] p)
        {
            int[] output = new int[p.Length];

            for (int x = 0; x < p.Length; x++)
            {
                output[p[p[p[x] - 1] - 1] - 1] = p[x];
            }

            return output;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int n = Convert.ToInt32(Console.ReadLine());

            int[] p = Array.ConvertAll(Console.ReadLine().Split(' '), pTemp => Convert.ToInt32(pTemp));
            int[] result = PermutationEquation(p);

            Console.WriteLine(string.Join("\n", result));

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
