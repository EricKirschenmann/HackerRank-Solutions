﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtopianTree
{
    class Program
    {
        // Complete the utopianTree function below.
        static int UtopianTree(int n)
        {
            int height = 1;

            // loop through total cycles
            for (int x = 0; x < n; x++)
            {
                // if odd cycle, just increase by 1
                if (x % 2 != 0)
                {
                    height++;
                }
                else
                {
                    // if even cycle double height
                    height *= 2;
                }
            }

            return height;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int t = Convert.ToInt32(Console.ReadLine());

            for (int tItr = 0; tItr < t; tItr++)
            {
                int n = Convert.ToInt32(Console.ReadLine());

                int result = UtopianTree(n);

                Console.WriteLine(result);
            }

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
