﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViralAdvertising
{
    class Program
    {
        // Complete the viralAdvertising function below.
        static int ViralAdvertising(int n)
        {
            // starts at 5 shares
            int shares = 5;
            int likes = 0;
            int cummulative = 0;

            // loop through n days
            for (int x = 0; x < n; x++)
            {
                // get the number of likes and count them
                likes = (int)Math.Floor(shares / 2.0);
                cummulative += likes;

                // increase the next days shares
                shares = likes * 3;
            }

            // return the total likes obtained over the course of n days
            return cummulative;
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int n = Convert.ToInt32(Console.ReadLine());

            int result = ViralAdvertising(n);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
