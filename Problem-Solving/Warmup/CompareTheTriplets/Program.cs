﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareTheTriplets
{
    class Program
    {
        // Complete the solve function below.
        static int[] Solve(int[] a, int[] b)
        {
            int p1 = 0;
            int p2 = 0;

            for (int x = 0; x < a.Length; x++)
            {
                if (a[x] > b[x])
                {
                    p1++;
                }
                else if (a[x] < b[x])
                {
                    p2++;
                }
            }

            return new int[] { p1, p2 };
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int[] a = Array.ConvertAll(Console.ReadLine().Split(' '), aTemp => Convert.ToInt32(aTemp))
            ;

            int[] b = Array.ConvertAll(Console.ReadLine().Split(' '), bTemp => Convert.ToInt32(bTemp))
            ;
            int[] result = Solve(a, b);

            Console.WriteLine(string.Join(" ", result));

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
