﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiagonalDifference
{
    class Program
    {
        // Complete the diagonalDifference function below.
        static int DiagonalDifference(int[][] a)
        {
            int diag1 = 0;
            int diag2 = 0;

            // go through row by row
            for (int x = 0; x < a.Length; x++)
            {
                diag1 += a[x][x];
                diag2 += a[x][a.Length - 1 - x];
            }

            return Math.Abs(diag1 - diag2);
        }

        static void Main(string[] args)
        {
            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int n = Convert.ToInt32(Console.ReadLine());

            int[][] a = new int[n][];

            for (int i = 0; i < n; i++)
            {
                a[i] = Array.ConvertAll(Console.ReadLine().Split(' '), aTemp => Convert.ToInt32(aTemp));
            }

            int result = DiagonalDifference(a);

            Console.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();
        }
    }
}
