﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMaxSum
{
    class Program
    {
        // Complete the miniMaxSum function below.
        static void MiniMaxSum(long[] arr)
        {
            // using LINQ
            long max = arr.Sum() - arr.Min();
            long min = arr.Sum() - arr.Max();

            Console.WriteLine($"{min} {max}");
        }

        static void Main(string[] args)
        {
            long[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt64(arrTemp));
            MiniMaxSum(arr);
        }
    }
}
