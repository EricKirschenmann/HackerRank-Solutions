﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlusMinus
{
    class Program
    {
        // Complete the plusMinus function below.
        static void PlusMinus(int[] arr)
        {
            double pos = 0;
            double neg = 0;
            double zer = 0;

            foreach (int item in arr)
            {
                if (item > 0)
                {
                    pos++;
                }
                else if (item < 0)
                {
                    neg++;
                }
                else
                {
                    zer++;
                }
            }

            Console.WriteLine($"{(pos / arr.Length):F6}");
            Console.WriteLine($"{(neg / arr.Length):F6}");
            Console.WriteLine($"{(zer / arr.Length):F6}");
        }

        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());

            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));

            PlusMinus(arr);
        }
    }
}
