﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Staircase
{
    class Program
    {
        // Complete the staircase function below.
        static void Staircase(int n)
        {
            for (int x = 0; x < n; x++)
            {
                Console.Write(new string(' ', (n - x - 1)));
                Console.WriteLine(new string('#', (x + 1)));
            }
        }

        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());

            Staircase(n);
        }
    }
}
