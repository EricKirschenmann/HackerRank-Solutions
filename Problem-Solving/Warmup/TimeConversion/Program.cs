﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeConversion
{
    class Program
    {
        /*
         * Complete the timeConversion function below.
         */
        static string TimeConversion(string s)
        {
            /*
             * Write your code here.
             */
            string[] split = s.Split(':');

            if (split[2].Contains("PM"))
            {
                // 12 PM doesn't become 24
                if (split[0] != "12")
                {
                    split[0] = (int.Parse(split[0]) + 12).ToString();
                }
                split[2] = split[2].Replace("PM", "");
            }
            else
            {
                // 12 AM becomes 00
                if (split[0] == "12")
                {
                    split[0] = "00";
                }
                split[2] = split[2].Replace("AM", "");
            }

            return string.Join(":", split);
        }

        static void Main(string[] args)
        {
            //TextWriter tw = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            string s = Console.ReadLine();

            string result = TimeConversion(s);

            Console.WriteLine(result);

            //tw.Flush();
            //tw.Close();
        }
    }
}
